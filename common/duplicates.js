module.exports = (arr, unique) => {
  const seen = [];
  const filtered = [];
  for (let i = 0; i < arr.length; i++) {
    const str = unique(arr[i]);
    if (!seen.includes(str)) {
      seen.push(str);
      filtered.push(arr[i]);
    }
  }
  return filtered;
}
