fs = require('fs');

module.exports = (filePath, convertToInts = false) => {
  return new Promise(
    (resolve, reject) => {
      fs.readFile(filePath, function (err, data) {
        if (err) {
          reject(err);
        }
        const d = data.toString()
          .trimEnd()
          .split('\n');
        if (convertToInts) {
          resolve(d.map(i => parseInt(i, 10)));
        } else {
          resolve(d);
        }
      });
    }
  )
}
