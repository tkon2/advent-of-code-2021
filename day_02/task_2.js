
const readfile = require('../common/readfile');

const solve = async () => {
  const raw = await readfile('./data.txt');
  const lines = raw.map(r =>  ({
    d: r.charAt(0),
    a: parseInt(r.replace(/^\w+ /, ''), 10)
  }));

  let x = 0;
  let y = 0;
  let aim = 0;
  for (let i = 0; i < lines.length; i++) {
    const line = lines[i];
    switch(line.d) {
      case 'd':
        aim += line.a;
        break;
      case 'f':
        x += line.a;
        y += line.a * aim;
        break;
      case 'u':
        aim -= line.a;
        break;
      default:
        console.log("Unexpected value: " + line.d);
    }
  }
  return x * y;
}

solve().then(v => console.log(v));
