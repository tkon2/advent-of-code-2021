const readfile = require('../common/readfile');

const solve = async () => {
  const lines = await readfile('./data.txt');


  const fix = (num) => {
    let depth = 0;
    const explode = (start) => {
      let end = start;
      while (num.charAt(end) !== ']') {
        end += 1;
      }
      const numbers = num.substring(start, end).split(',').map(n => parseInt(n, 10));
      // Rightmost action first to preserve string indexes
      let r = end;
      while (r < num.length && ['[', ']', ','].includes(num.charAt(r))) {
        r += 1;
      }
      if (r < num.length) {
        const Nr = parseInt(num.substring(r), 10);
        const Ne = r + `${Nr}`.length;
        num = num.substring(0, r) + (Nr + numbers[1]) + num.substring(Ne);
      }

      // Replace original number with the number 0
      num = num.substring(0, start - 1) + '0' + num.substring(end + 1);

      let l = start - 2;
      while (l >= 0 && ['[', ']', ','].includes(num.charAt(l))) {
        l -= 1;
      }
      // We DON'T know it's a single digit number!
      while (l >= 1 && !['[', ']', ','].includes(num.charAt(l - 1))) {
        l -= 1;
      }

      if (l >= 0) {
        const Nl = parseInt(num.substring(l));
        const Ne = l + `${Nl}`.length;
        num = num.substring(0, l) + (Nl + numbers[0]) + num.substring(Ne);
      }
    }

    const split = (pos) => {
      const N = parseInt(num.substring(pos), 10);
      const Ne = pos + `${N}`.length;
      num = num.substring(0, pos)
        + `[${Math.floor(N / 2)},${Math.ceil(N / 2)}]`
        + num.substring(Ne);
    }

    outer: while (true) {
      depth = 0;
      for (let i = 0; i < num.length; i++) {
        if (num.charAt(i) === '[') {
          depth += 1;
        } else if (num.charAt(i) === ']') {
          depth -= 1;
        } else if (num.charAt(i) === ',') {
          continue;
        } else {
          if (depth >= 5 && /^\[\d+,\d+\]/.test(num.substring(i - 1))) {
            explode(i);
            continue outer;
          }
        }
      }

      for (let i = 0; i < num.length; i++) {
        if (i === num.length - 1) {
          break outer;
        }
        if (['[', ']', ','].includes(num.charAt(i))) {
          continue;
        }
        value = parseInt(num.substring(i), 10);
        if (value >= 10) {
          split(i);
          continue outer;
        }
      }
    }
    return num;
  }

  const sum = (num) => {
    let depth = Infinity;
    let maxTries = 1000;
    while (depth > 1 && maxTries > 0) {
      maxTries -= 1;
      depth = 0;
      for (let start = 0; start < num.length; start++) {
        if (num.charAt(start) === '[') {
          depth += 1;
        }
        if (/^\[\d+,\d+\]/.test(num.substring(start))) {
          start += 1;
          let end = start;
          while (num.charAt(end) !== ']') {
            end += 1;
          }
          const nums = num.substring(start, end).split(',').map(v => parseInt(v, 10));
          num = num.substring(0, start - 1)
            + (nums[0] * 3 + nums[1] * 2)
            + num.substring(end + 1);
        }
      }
    }
    return maxTries > 0 ? parseInt(num, 10) : num;

  }

  if (lines.length <= 0) {
    return;
  }

  let max = 0;
  for (let i = 0; i < lines.length - 1; i++) {
    for (let j = i + 1; j < lines.length; j++) {
      const num1 = fix(`[${lines[i]},${lines[j]}]`);
      const ans1 = sum(num1);
      const num2 = fix(`[${lines[j]},${lines[i]}]`);
      const ans2 = sum(num2);
      max = Math.max(ans1, ans2, max);
    }
  }

  return max;
}

solve().then(v => console.log(v));
