const readfile = require('../common/readfile');

const solve = async () => {
  const lines = await readfile('./data.txt', true);

  let last = null;
  let total = 0;
  for (let i = 0; i < lines.length; i++) {
    const line = lines[i];
    if (last === null) {
      ;
    } else if (line > last) {
      total ++;
    }
    last = line;
  }
  return total;
}

solve().then(v => console.log(v));
