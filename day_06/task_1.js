const readfile = require('../common/readfile');



const solve = async () => {
  const raw = await readfile('./data.txt');
  
  let fishes = raw[0].split(',');

  for (let i = 0; i < 80; i++) {
    const nextFish = [];
    for (let f = 0; f < fishes.length; f++) {
      const fish = fishes[f];
      if (fish === 0) {
        nextFish.push(6); // itself
        nextFish.push(8); // new fish
      } else {
        nextFish.push(fish - 1);
      }
    }
    fishes = nextFish;
  }
  return fishes.length;
}

solve().then(v => console.log(v));
