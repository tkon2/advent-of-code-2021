const readfile = require('../common/readfile');

const solve = async () => {
  const raw = await readfile('./data.txt');
  
  let inputfishes = raw[0].split(',');
  let fishes = [];
  for (let i = 0; i < inputfishes.length; i++) {
    const fish = parseInt(inputfishes[i]);
    fishes[fish] = !fishes[fish] ? 1 : fishes[fish] + 1;
  }

  for (let i = 0; i < 256; i++) {
    const nextFish = [];
    for (let f = 0; f < fishes.length; f++) {
      const fish = fishes[f] || 0;
      if (fish === 0) continue;
      if (f === 0) {
        if (fish) {
          nextFish[6] = !nextFish[6] ? fish : nextFish[6] + fish;
          nextFish[8] = !nextFish[8] ? fish : nextFish[8] + fish;
        }
      } else {
        nextFish[f - 1] = !nextFish[f - 1] ? fish : nextFish[f - 1] + fish;
      }
    }
    fishes = nextFish;
  }
  return fishes.reduce((acc, curr) => curr ? acc + curr : acc, 0);
}

solve().then(v => console.log(v));
