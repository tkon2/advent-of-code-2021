const readfile = require('../common/readfile');
const removeDupes = require('../common/duplicates');

const print = (arr) => {
  for (let i = 0; i < 6; i++) {
    let line = '';
    for (let j = 0; j < 39; j++) {
      if (arr.some(a => a[0] === j && a[1] === i)) {
        line = line + '#'
      } else {
        line = line + ' '
      }
    }
    console.log(line);
  }
}

const solve = async () => {
  const lines = await readfile('./data.txt');
  const F = lines.filter(l => l.includes('fold')); // Folds
  let B = lines.filter(l => !l.includes('fold') && l !== ''); // Board
  B = B.map(b => b.split(',').map(bb => parseInt(bb, 10)));

  for (let f = 0; f < F.length; f++) {

    const fold = F[f];
    const vertical = fold.includes('y');
    const n = parseInt(fold.split('=')[1], 10);
    for (let b = 0; b < B.length; b++) {
      if (vertical) {

        if (B[b][1] > n) {
          const amplitude = Math.abs(B[b][1] - n);
          B[b][1] = (n - amplitude);
        }
      } else {

        if (B[b][0] > n) {
          const amplitude = Math.abs(B[b][0] - n);
          B[b][0] = (n - amplitude);
        }
      }
    }
    B = removeDupes(B, b => `${b[0]},${b[1]}`);
  }
  
  print(B);
}

solve();
