#! /bin/bash


# Check required programs are installed

command_exists() {
    command -v "$1" >/dev/null 2>&1
    if [[ $? -ne 0 ]]; then
        echo "$1 is required to run the solutions"
        exit 1
    fi
}

for COMMAND in "python3" "node"; do
    command_exists "${COMMAND}"
done


# Run solutions

(cd day_01 && printf "Day 01 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))
(cd day_02 && printf "Day 02 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))
(cd day_03 && printf "Day 03 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))
(cd day_04 && printf "Day 04 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))
(cd day_05 && printf "Day 05 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))
(cd day_06 && printf "Day 06 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))
(cd day_07 && printf "Day 07 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))
(cd day_08 && printf "Day 08 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))
(cd day_09 && printf "Day 09 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))
(cd day_10 && printf "Day 10 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))
(cd day_11 && printf "Day 11 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))
(cd day_12 && printf "Day 12 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))
(cd day_13 && printf "Day 13 task 1: \e[36m%-8d\e[0m task 2: \e[36mRead below V\e[0m\n" $(node ./task_1.js); node ./task_2.js)
(cd day_14 && printf "Day 14 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))
(cd day_15 && printf "Day 15 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))
(cd day_16 && printf "Day 16 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))
(cd day_17 && printf "Day 17 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))
(cd day_18 && printf "Day 18 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))
(cd day_19 && printf "Day 19 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))
(cd day_20 && printf "Day 20 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))
(cd day_21 && printf "Day 21 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))


(cd day_24 && printf "Day 24 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))
(cd day_25 && printf "Day 25 task 1: \e[36m%-8d\e[0m task 2: \e[36m%d\e[0m\n" $(node ./task_1.js) $(node ./task_2.js))
