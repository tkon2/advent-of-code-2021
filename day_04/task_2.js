const readfile = require('../common/readfile');

const DATA_WIDTH = 12;

const boardSolved = (board) => {
  for (let i = 0; i < board.length; i++) {
    const lineSolved = board[i][0].called 
      && board[i][1].called 
      && board[i][2].called 
      && board[i][3].called 
      && board[i][4].called;
    
    const rowSolved = board[0][i].called 
      && board[1][i].called 
      && board[2][i].called 
      && board[3][i].called 
      && board[4][i].called;

    if (lineSolved || rowSolved) {
      return true;
    }
  }
  return false;
}

const markCalled = (board, number) => {
  return board.map(b => {
    return b.map(r => {
      if (number === r.number) {
        return {...r, called: true};
      }
      return {...r};
    })
  })
}


const caclulateScore = (board) => {
  let score = 0;
  for (let i = 0; i < board.length; i++) {
    for (let j = 0; j < board.length; j++) {
      const num = board[i][j];
      if (!num.called) {
        score += num.number;
      }
    }
  }
  return score;
}

const showBoard = (board) => {
  const lines = board.map(r => r.map(l => `${l.number}${l.called ? '!' : ''}`));
  console.log(lines);
}

const solve = async () => {
  const raw = await readfile('./data.txt');
  const numberChain = raw[0].split(',').map(n => parseInt(n, 10));
  raw.shift();
  raw.shift(); // take out the newline

  const boards = [[]];
  for (let i = 0; i < raw.length; i++) {
    const line = raw[i];
    if (line === '') {
      boards.push([]);
    } else {
      boards[boards.length - 1].push(
        line.replace(/^ +/, '')
          .split(/ +/)
          .map(l => ({called: false, number: parseInt(l, 10)}))
      )
    }
  }


  for (let i = 0; i < numberChain.length; i++) {
    const number = numberChain[i];
    for (let b = 0; b < boards.length; b++) {
      boards[b] = markCalled(boards[b], number);
      const board = boards[b];
      const winningBoards = boards.reduce((acc, curr) => acc + (boardSolved(curr) ? 1 : 0), 0);
      if (winningBoards === boards.length) {
        return caclulateScore(board) * number;
      }
    }
  }
  return null;
}

solve().then(v => console.log(v));
