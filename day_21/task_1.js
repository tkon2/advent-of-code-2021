const readfile = require('../common/readfile');

const solve = async () => {
  const lines = await readfile('./data.txt');

  players = lines.map(l => parseInt(l.split(': ')[1], 10));
  scores = Array.from({ length: players.length }, () => 0);

  let totalRolls = 0;
  let dice = 1;

  const roll = (num) => {
    let number = 0;
    for (let i = 0; i < num; i++) {
      number += dice;
      totalRolls += 1;
      dice += 1;
      if (dice > 100) {
        dice = 1;
      }
    }
    return number;
  }

  while (!scores.some(s => s >= 1000)) {
    for (let i = 0; i < players.length; i++) {
      players[i] += roll(3);
      while (players[i] > 10) {
        players[i] -= 10;
      }
      scores[i] += players[i];
      if (scores[i] >= 1000) {
        break;
      }
    }
  }

  return scores.find(s => s < 1000) * totalRolls;
}

solve().then(v => console.log(v));
