const readfile = require('../common/readfile');

const solve = async () => {
  const lines = await readfile('./data.txt');

  players = lines.map(l => [parseInt(l.split(': ')[1], 10), 0]);
  // [[position,score],[position,score]]

  let futures = [[1, 0, 0, ...players]];
  // Total number of copies of future,
  // Total rolls for this future,
  // Turn counter,
  // Players for this future

  let finishedFutures = Array.from({ length: players.length }, () => 0);

  const sameFate = (a, b) => {
    if (a[1] !== b[1]) {
      return false;
    } else if (a[2] !== b[2]) {
      return false;
    }
    if (a.length != b.length) {
      return false;
    }
    for (let i = 3; i < a.length; i++) {
      if (a[i][0] !== b[i][0]) {
        return false;
      } else if (a[i][1] !== b[i][1]) {
        return false;
      }
    }
    return true;
  }

  const addFuture = (f) => { // New Future
    for (let i = 0; i < futures.length; i++) {
      if (sameFate(futures[i], f)) {
        futures[i][0] += f[0]; // Increase future count
        return;
      }
    }
    futures.push(f);
  }

  const roll3 = () => {
    return [
      3, 4, 4, 4, 5, 5, 5, 5, 5,
      5, 6, 6, 6, 6, 6, 6, 6, 7,
      7, 7, 7, 7, 7, 8, 8, 8, 9
    ];
  }

  const cpFuture = (f) => {
    return f.map((p, i) => i < 3 ? p : [...p]);
  }

  while (futures.length) {
    let future = futures.pop();
    const player = future[2];
    const playerI = player + 3;
    const newPlayer = (player + 1) % players.length;
    const potentialRolls = roll3();
    for (let r = 0; r < potentialRolls.length; r++) {
      const newFuture = cpFuture(future);
      newFuture[1] += 3;
      newFuture[2] = newPlayer;
      newFuture[playerI][0] += potentialRolls[r]

      while (newFuture[playerI][0] > 10) {
        newFuture[playerI][0] -= 10;
      }

      newFuture[playerI][1] += newFuture[playerI][0];
      if (newFuture[playerI][1] >= 21) {
        finishedFutures[player] += newFuture[0];
      } else {
        addFuture(newFuture);
      }
    }
  }
  return finishedFutures.reduce((a, c) => Math.max(a, c), 0);

}

solve().then(v => console.log(v));
