const readfile = require('../common/readfile');

const charDiff = (str1, str2) => {
  if (str1.length < str2.length) {
    const temp = str2;
    str2 = str1;
    str1 = temp;
  }
  let output = str1;
  for (let i = 0; i < str2.length; i++) {
    output = output.replace(str2.charAt(i), '');
  }
  return output;
}

const strContainsAny = (base, str) => {
  for (let i = 0; i < str.length; i++) {
    if (base.includes(str.charAt(i))) return true;
  }
  return false;
}

const strContainsAll = (base, str) => {
  for (let i = 0; i < str.length; i++) {
    if (!base.includes(str.charAt(i))) return false;
  }
  return true;
}

const sortChars = (str) => {
  return str.split('').sort().join('');
}

const solve = async () => {
  const raw = await readfile('./data.txt');
  
  const groups = raw.map(r => r.split(' | '));
  const prePipe = groups.map(g => g[0]).map(g => g.split(' '));
  const postPipe = groups.map(g => g[1]).map(g => g.split(' '));

  let total = 0;
  for (let i = 0; i < prePipe.length; i++) {
    prePipe[i] = prePipe[i].sort((a,b) => a.length - b.length);

    const nums = prePipe[i];

    const num1 = nums[0];
    const num4 = nums[2];
    const num7 = nums[1];
    const num8 = nums[9];
    const num096 = [nums[6], nums[7], nums[8]];
    const num235 = [nums[3], nums[4], nums[5]];

    const num3 = num235.filter(n => strContainsAll(n, num1))[0];
    const num25 = num235.filter(n => n !== num3);
    const num5 = num25.filter(n => charDiff(n, num4).length === 2)[0];
    const num2 = num25.filter(n => charDiff(n, num4).length === 3)[0];

    const num6 = num096.filter(n => charDiff(n, num1).length === 5)[0];
    const num09 = num096.filter(n => charDiff(n, num1).length !== 5);
    const num0 = num09.filter(n => charDiff(n, num4).length === 3)[0];
    const num9 = num09.filter(n => charDiff(n, num4).length !== 3)[0];

    const numbers = [
      sortChars(num0),
      sortChars(num1),
      sortChars(num2),
      sortChars(num3),
      sortChars(num4),
      sortChars(num5),
      sortChars(num6),
      sortChars(num7),
      sortChars(num8),
      sortChars(num9)
    ];

    let lineNum = '';
    const toSolve = postPipe[i];
    let sum = 0;
    for (let j = 0; j < toSolve.length; j++ ) {
      const index = numbers.indexOf(sortChars(toSolve[j]));
      if (index == -1) {
        console.error("SOMETHING WENT WRONG!!")
      }
      lineNum += `${index}`;
    }

    total += parseInt(lineNum);
    
  }
  return total;

}

solve().then(v => console.log(v));
