const readfile = require('../common/readfile');

const solve = async () => {
  const raw = await readfile('./data.txt');
  
  const groups = raw.map(r => r.split(' | '));
  const prePipe = groups.map(g => g[0]).map(g => g.split(' '));
  const postPipe = groups.map(g => g[1]).map(g => g.split(' '));

  let charCount = 0;
  for (let i = 0; i < postPipe.length; i++) {
    for (let j = 0; j < postPipe[i].length; j++) {
      const l = postPipe[i][j].length;
      if (l === 7 || l === 2 || l === 4 || l === 3)  {
        charCount += 1;
      }
    }
  }
  return charCount;

}

solve().then(v => console.log(v));
