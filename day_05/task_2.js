const readfile = require('../common/readfile');

const MAX_WIDTH = 1000;
const MAX_HEIGHT = 1000;


const solve = async () => {
  const raw = await readfile('./data.txt');
  const lines = [];
  for (let i = 0; i < raw.length; i++) {
    const captures = raw[i].match(/(\d+),(\d+) -> (\d+),(\d+)/);
    lines.push({
      x1: parseInt(captures[1]),
      y1: parseInt(captures[2]),
      x2: parseInt(captures[3]),
      y2: parseInt(captures[4]),
    })
  }

  const positions = [];
  for (let i = 0; i < lines.length; i++) {
    const line = lines[i];
    if (line.x1 === line.x2) {
      const start = Math.min(line.y1, line.y2);
      const stop = Math.max(line.y1, line.y2);
      for (let y = start; y <= stop; y++) {
        const index = line.x1 + (y * MAX_WIDTH);
        positions[index] = positions[index] ? positions[index] + 1 : 1;
      }
    } else if (line.y1 === line.y2) {
      const start = Math.min(line.x1, line.x2);
      const stop = Math.max(line.x1, line.x2);
      for (let x = start; x <= stop; x++) {
        const index = x + (line.y1 * MAX_WIDTH);
        positions[index] = positions[index] ? positions[index] + 1 : 1;
      }
    } else {
      const diff = Math.abs(line.x1 - line.x2);
      const xd = (line.x2 - line.x1) / diff;
      const yd = (line.y2 - line.y1) / diff;
      const xs = line.x1;
      const ys = line.y1;
      for (let i = 0; i <= diff; i++) {
        const x = xs + (i * xd);
        const y = ys + (i * yd);
        const index = x + (y * MAX_WIDTH);
        positions[index] = positions[index] ? positions[index] + 1 : 1;
      }
    }
  }

  return positions.reduce((acc, curr) => acc += (curr && curr >= 2) ? 1 : 0, 0);
  
}

solve().then(v => console.log(v));
