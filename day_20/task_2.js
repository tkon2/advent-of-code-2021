const readfile = require('../common/readfile');

const solve = async () => {
  const lines = await readfile('./data.txt');

  const enhancement = lines[0].split('');

  let image = lines.slice(2).map(l => l.split(''));

  const getIndex = (x, y, flip = false) => {
    let str = '';
    for (let i = -1; i < 2; i++) {
      for (let j = -1; j < 2; j++) {
        const Yn = y + i;
        const Xn = x + j;
        if (Yn < 0 || Yn >= image.length) {
          str += flip ? '1' : '0';
        } else if (Xn < 0 || Xn >= image[0].length) {
          str += flip ? '1' : '0';
        } else {
          str += image[Yn][Xn] === '#' ? '1' : '0';
        }
      }
    }
    return parseInt(str, 2);
  }


  for (let r = 0; r < 50; r++) {
    let newImage = Array.from(
      { length: image.length + 2 },
      () => Array.from({ length: image[0].length + 2 }, () => '.')
    );
    for (let i = -1; i <= image.length; i++) {
      for (let j = -1; j <= image[0].length; j++) {
        const n = getIndex(i, j, enhancement[0] === '#' ? r % 2 === 1 : false);
        newImage[j + 1][i + 1] = enhancement[n];
      }
    }
    image = newImage;
  }

  const litCount = image.reduce(
    (a, c) => a + c.reduce(
      (aa, cc) => aa + (cc === '#' ? 1 : 0),
      0
    ),
    0
  );


  return litCount;

}

solve().then(v => console.log(v));
