const readfile = require('../common/readfile');

const DATA_WIDTH = 12;

const mostCommonAt = (arr, pos) => {
  const count = arr.reduce((acc, curr) => acc + (curr.charAt(pos) === '1'), 0);
  return 0 + (count < arr.length / 2);
}

const solveFor = (data, mode) => {
  let arr = data;
  for (let i = 0; i < DATA_WIDTH; i++) {
    const mostCommon = mostCommonAt(arr, i);
    const shouldDelete = mode ? !!mostCommon : !mostCommon;
    arr = arr.filter(a => (shouldDelete ? '1' : '0') === a.charAt(i) ); 
    if (arr.length === 1) {
      return parseInt(arr[0], 2);
    }
    if (arr.length === 0) {
      throw new Error("Where is the rum gone?");
    }
  }
}

const solve = async () => {
  const raw = await readfile('./data.txt');
  const production = solveFor(raw, true);
  const consumption = solveFor(raw, false);
  const ans = production * consumption;
  return ans;
}

solve().then(v => console.log(v));
