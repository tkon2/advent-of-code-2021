const readfile = require('../common/readfile');

const DATA_WIDTH = 12;

const solve = async () => {
  const raw = await readfile('./data.txt');
  const weights = raw.reduce((acc, curr) => {
    const weight = [];
    for (i = 0; i < DATA_WIDTH; i++) {
      weight.push(acc[i] + (curr.charAt(i) === '1'));
    } 
    return weight;
   }, Array.from(new Array(DATA_WIDTH)).map(() => 0));

   const gamma = weights.map(w => 0 + (w > raw.length / 2)).join('');
   const epsilon = weights.map(w => 0 + (w < raw.length / 2)).join('');
   const ans = parseInt(gamma, 2) * parseInt(epsilon, 2);
   return ans;
}

solve().then(v => console.log(v));
