const readfile = require('../common/readfile');

const orientations = [
  [0, 1, 2], // X Y Z
  [1, 2, 0], // Y Z X
  [2, 0, 1], // Z X Y
  [2, 1, 0], // Z Y X
  [1, 0, 2], // Y X Z
  [0, 2, 1], // X Z Y
];

const flips = [
  [1, 1, 1],   // +X +Y +Z
  [1, 1, -1],  // +X +Y -Z 
  [1, -1, 1],  // +X -Y +Z
  [1, -1, -1], // +X -Y -Z
  [-1, 1, 1],  // -X +Y +Z
  [-1, 1, -1], // -X +Y -Z
  [-1, -1, 1], // -X -Y +Z
  [-1, -1, -1] // -X -Y -Z
]

const permutations = orientations.length * flips.length;

const permute = (coords, p) => {
  const orientPos = p % orientations.length
  const flipPos = Math.floor(p / (orientations.length))
  const orientation = orientations[orientPos];
  const flip = flips[flipPos];
  return [
    flip[0] * coords[orientation[0]],
    flip[1] * coords[orientation[1]],
    flip[2] * coords[orientation[2]],
  ]
}

posEqual = (a, b) => {
  if (!a || !b) return 0;
  return a[0] === b[0] && a[1] === b[1] && a[2] === b[2];
}

const getMatches = (a, b) => {
  let matches = [];
  for (let i = 0; i < a.length; i++) {
    for (let j = 0; j < b.length; j++) {
      if (posEqual(a[i], b[j])) {
        matches.push(a[i]);
      }
    }
  }
  return matches;
}

const getKnown = (a, b) => {
  for (let p = 0; p < permutations; p++) {
    for (let i = 0; i < a.length; i++) {
      for (let j = 0; j < b.length; j++) {
        const perm = permute(b[j], p);
        const Xoff = a[i][0] - perm[0];
        const Yoff = a[i][1] - perm[1];
        const Zoff = a[i][2] - perm[2];
        const bOff = b.map(bi => {
          const perms = permute(bi, p);
          return [
            perms[0] + Xoff,
            perms[1] + Yoff,
            perms[2] + Zoff,
          ]
        })
        const matches = getMatches(a, bOff);
        if (matches.length >= 12) {
          return {
            pos: [Xoff, Yoff, Zoff],
            dat: bOff,
          }
        }
      }
    }
  }
  return null;
}


const countBeacons = (L) => {
  const beacons = [];
  for (let i = 0; i < L.length; i++) {
    const l = L[i].dat;
    for (let j = 0; j < l.length; j++) {
      const pos = l[j];
      if (!beacons.some(b => posEqual(b, pos))) {
        beacons.push(pos);
      }
    }
  }
  return beacons.length;
}

const solve = async () => {
  const lines = await readfile('./data.txt');

  // Deal with input
  const scanners = [];
  let scannerCount = 0;
  for (let i = 0; i < lines.length; i++) {
    const line = lines[i];
    if (line.includes('--- scanner')) {
      scanners.push([]);
      scannerCount += 1;
      continue;
    } else if (line === '') {
      continue;
    }

    scanners[scanners.length - 1].push(
      line.split(',').map(l => parseInt(l, 10))
    )
  }

  const known = [
    { pos: [0, 0, 0], dat: [...scanners[0]] }
  ]
  let remaining = scanners.slice(1);

  while (remaining.length) {
    let found = false;
    const r = remaining.pop();
    for (let i = 0; i < known.length; i++) {
      k = known[i];
      const newK = getKnown(k.dat, r);
      if (newK) {
        found = true;
        known.push(newK);
        break;
      }
    }
    if (!found) {
      remaining = [r, ...remaining];
    }
  }

  return countBeacons(known);

}

solve().then(v => console.log(v));
