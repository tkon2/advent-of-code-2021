const readfile = require('../common/readfile');

const solve = async () => {
  const lines = await readfile('./data.txt');
  let line = lines[0]; // single line

  line = line.substring(13);

  const xy = line.split(', ')
    .map(i => (
      i.substring(2).split('..')
        .map(j => parseInt(j, 10))
        .sort((a, b) => a - b)
    ));

  const Gx = xy[0];
  const Gy = xy[1];

  const inGoal = (Vx, Vy) => {
    if (Vx >= Gx[0] && Vx <= Gx[1]) {
      if (Vy >= Gy[0] && Vy <= Gy[1]) {
        return true;
      }
    }
    return false;
  }

  const pastGoal = (Vx, Vy) => {
    if (
      Vx > Gx[1] || Vy < Gy[0]) { // since Gy is negative
      return true;
    }
    return false;
  }

  /**
   * Gives -1 if it's past goal,
   * 0 if it's still in play,
   * and 1 if it's in the goal
   */
  test = (Vx, Vy) => {
    if (pastGoal(Vx, Vy)) {
      return -1;
    } else if (inGoal(Vx, Vy)) {
      return 1;
    } else {
      return 0;
    }
  }


  launch = (vx, vy) => {
    let X = 0;
    let Y = 0;
    let Vx = vx;
    let Vy = vy;

    while (true) {
      const result = test(X, Y);
      if (result === -1) {
        return false;
      } else if (result === 1) {
        return true;
      }
      if (Vx === 0 && X < Gx[0]) {
        return false;
      }
      if (Vx === 0 && Y < Gy[0]) {
        return false;
      }
      // Apply rules
      X = X + Vx;
      Y = Y + Vy;
      Vx = Math.max(0, Vx - 1); // No point going backwards.. ever!
      Vy = Vy - 1;
    }
  }

  let count = 0;
  for (let i = -100; i < Gx[1]; i++) { // Y
    for (let j = 1; j < 1000; j++) { // X
      const hit = launch(j, i);
      if (hit) {
        count += 1;
      }
    }
  }

  return count;

}

solve().then(v => console.log(v));
