const readfile = require('../common/readfile');

const solve = async () => {
  const raw = await readfile('./data.txt');
  
  const board = raw.map(r => r.split('').map(i => parseInt(i, 10)));

  let lowPoints = 0;
  for (let i = 0; i < board.length; i++) {
    for (let j = 0; j < board[i].length; j++) {
      const h = board[i][j];
      const neighbours = [];
      if (i - 1 >= 0) {
        neighbours.push(board[i-1][j]);
      }
      if (i + 1 < board.length) {
        neighbours.push(board[i+1][j]);
      }
      if (j - 1 >= 0) {
        neighbours.push(board[i][j-1]);
      }
      if (j + 1 < board[i].length) {
        neighbours.push(board[i][j+1]);
      }

      const lowestNeighbour = neighbours.reduce((acc, curr) => Math.min(acc, curr), Infinity);
      if (h < lowestNeighbour) {
        lowPoints += h + 1;
      }
    }
  }
  return lowPoints;
}

solve().then(v => console.log(v));
