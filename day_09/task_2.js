const readfile = require('../common/readfile');

const solve = async () => {
  const raw = await readfile('./data.txt');
  
  const board = raw.map(r => r.split('').map(i => parseInt(i, 10)));

  const gnp = (x,y) => { // Get neighbour positions
    const neighbours = [];
    if (x - 1 >= 0) {
      neighbours.push([x-1, y]);
    }
    if (x + 1 < board.length) {
      neighbours.push([x+1, y]);
    }
    if (y - 1 >= 0) {
      neighbours.push([x, y-1]);
    }
    if (y + 1 < board[x].length) {
      neighbours.push([x, y+1]);
    }
    return neighbours;
  }
  const gn = (x,y) => { // Get neighbour (values)
    const neighbours = [];
    if (x - 1 >= 0) {
      neighbours.push(board[x-1][y]);
    }
    if (x + 1 < board.length) {
      neighbours.push(board[x+1][y]);
    }
    if (y - 1 >= 0) {
      neighbours.push(board[x][y-1]);
    }
    if (y + 1 < board[x].length) {
      neighbours.push(board[x][y+1]);
    }
    return neighbours;
  }

  let basinOrigins = [];
  for (let i = 0; i < board.length; i++) {
    for (let j = 0; j < board[i].length; j++) {
      const h = board[i][j];
      const neighbours = gn(i,j);

      const lowestNeighbour = neighbours.reduce((acc, curr) => Math.min(acc, curr), Infinity);
      if (h < lowestNeighbour) {
        basinOrigins.push([i,j]);
      }
    }
  }

  let basinSizes = [];

  for (let b = 0; b < basinOrigins.length; b++) {
    let basinToCheck = [basinOrigins[b]];
    let basinSize = 1;
    let i = 0;
    while (i < basinToCheck.length) {
      const basin = basinToCheck[i];
      i += 1;
      let neighbours = gnp(basin[0], basin[1]);
      neighbours = neighbours.filter(n => board[n[0]][n[1]] !== 9);
      neighbours = neighbours.filter(n => !basinToCheck.some(b => b.join('') === n.join('')))
      basinSize += neighbours.length;
      basinToCheck = [...basinToCheck, ...neighbours];
    }
    basinSizes.push(basinSize);

  }
  basinSizes.sort((a,b) => b-a);
  return basinSizes[0] * basinSizes[1] * basinSizes[2];
}

solve().then(v => console.log(v));
