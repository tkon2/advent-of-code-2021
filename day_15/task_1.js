const readfile = require('../common/readfile');

const solve = async () => {
  const lines = await readfile('./data.txt');
  let G = lines.map(l => l.split('').map(ll => parseInt(ll, 10)))

  const r = G.length; // Rows
  const c = G[0].length; // Cols

  const nx = [-1, 0, 1, 0];
  const ny = [0, 1, 0, -1];

  const s = (t) => {
    const w = t * r;
    const h = t * c;
    let D = Array.from({ length: w }, () => Array.from({ length: h }, () => null));
    let F = [[0, 0, 0]]; // Frontier -- cost, x, y
    while (F.length) {
      const [cost, x, y] = F.pop();
      if (x < 0 || x >= w || y < 0 || y >= h) {
        // Out of bounds
        continue;
      }
      let v = G[x % r][y % c] + Math.floor(x / r) + Math.floor(y / c);
      while (v > 9) {
        v -= 9;
      }

      const currCost = (cost + v);

      if (D[x][y] === null || currCost < D[x][y]) {
        D[x][y] = currCost;
      } else {
        // Position already has cheaper path
        continue;
      }
      if (x === w - 1 && y === h - 1) {
        // We've calculated the final position
        break;
      }
      for (let i = 0; i < nx.length; i++) {
        const xx = nx[i] + x;
        const yy = ny[i] + y;
        F.push([D[x][y], xx, yy]);
      }
      F = F.sort((a, b) => b[0] - a[0]);
    }
    return D[w - 1][h - 1] - G[0][0];
  }

  return s(1);
}

solve().then(v => console.log(v));
