
const readfile = require('../common/readfile');

const openers = ['{', '(', '<', '['];
const closers = ['}', ')', '>', ']'];
const otc = {
  '(': ')',
  '{': '}',
  '<': '>',
  '[': ']',
}
const points = {
  '(': 1,
  '[': 2,
  '{': 3,
  '<': 4,
}

const solve = async () => {
  const lines = await readfile('./data.txt');
  let nonError = [];
  
  for (let i = 0; i < lines.length; i++) {
    const order = [];
    let valid = true;
    for (let j = 0; j < lines[i].length && valid; j++) {
      const char = lines[i].charAt(j);
      if (openers.includes(char)) order.push(char);
      if (closers.includes(char)) {
        if (otc[order.pop()] !== char) {
          valid = false;
        }
      }
    }
    if (valid) {
      order.reverse();
      nonError.push([...order]);
    }
  }


  const scores = [];
  for(let i = 0; i < nonError.length; i++) {
    let localScore = 0;
    for (let j = 0; j < nonError[i].length; j++) {
      const char = nonError[i][j];
      localScore *= 5;
      localScore += points[char];
    }
    scores.push(localScore);

  }
  scores.sort((a,b) => a-b);
  return(scores[(scores.length - 1) / 2]);
}

solve().then(v => console.log(v));


