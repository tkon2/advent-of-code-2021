const readfile = require('../common/readfile');

const openers = ['{', '(', '<', '['];
const closers = ['}', ')', '>', ']'];
const otc = {
  '(': ')',
  '{': '}',
  '<': '>',
  '[': ']',
}
const points = {
  ')': 3,
  ']': 57,
  '}': 1197,
  '>': 25137,
}
const solve = async () => {
  const lines = await readfile('./data.txt');
  
  let total = 0;
  for (let i = 0; i < lines.length; i++) {
    const order = [];
    let valid = true;
    for (let j = 0; j < lines[i].length && valid; j++) {
      const char = lines[i].charAt(j);
      if (openers.includes(char)) order.push(char);
      if (closers.includes(char)) {
        if (otc[order.pop()] !== char) {
          valid = false;
          total += points[char];
        }
      }
    }
  }
  return total;
}

solve().then(v => console.log(v));
