const readfile = require('../common/readfile');

const canEnterRoom = (A, i) => { // Array, item
  if (i === 'start') {
    // Cannot return to start
    return false;
  }
  if (i !== i.toLowerCase()) {
    // i is uppercase, can always be entered
    return true;
  }

  // Make a smaller array if just the stuff in question - A small
  const As = A.filter(a => a === a.toLowerCase() && a !== 'start');

  // Check if item appears, if it doesn't then allow it
  const count = As.filter(a => a === i).length || 0;
  if (count <= 0) {
    return true;
  }

  // Apply special rule *once*
  for (let j = 0; j < As.length; j++) {
    const count = As.filter(a => a === As[j]).length || 0;
    if (count >= 2) {
      // Some item appears twice
      return false;
    }
  }
  return true;
}

const solve = async () => {
  const lines = await readfile('./data.txt');
  const d = lines.map(l => l.split('-'));

  let pathCount = 0;
  let paths = [['start']];

  while(paths.length) {
    const nextPaths = [];
    for (let p = 0; p < paths.length; p++) {
      const currentPath = paths[p];
      const currentRoom = currentPath[currentPath.length-1]
      for (let i = 0; i < d.length; i++) {
        let start = d[i][0];
        let end = d[i][1];
        if (end === currentRoom) {
          // Swap them
          start = d[i][1];
          end = d[i][0];
        }
        if (start === currentRoom && canEnterRoom(currentPath, end)) {
          // Room can be entered
          if (end === 'end') {
            pathCount += 1;
          } else {
            nextPaths.push([...currentPath, end]);
          }
        }
      }
    }
    paths = [...nextPaths];
  }
  
  return pathCount;
}

solve().then(v => console.log(v));
