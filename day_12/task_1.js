const readfile = require('../common/readfile');

const solve = async () => {
  const lines = await readfile('./data.txt');
  const d = lines.map(l => l.split('-'));

  let pathCount = 0;
  let paths = [['start']];

  while(paths.length) {
    const nextPaths = [];
    for (let p = 0; p < paths.length; p++) {
      const currentPath = paths[p];
      const currentRoom = currentPath[currentPath.length-1]
      if (currentRoom === 'end') {
        // We are at the end of a successful path
        pathCount += 1;
        continue;
      }
      for (let i = 0; i < d.length; i++) {
        let start = d[i][0];
        let end = d[i][1];
        if (end === currentRoom) {
          // Swap them
          start = d[i][1];
          end = d[i][0];
        }
        if (start === currentRoom) {
          if (end === end.toLowerCase() && currentPath.includes(end)) {
            // room is lowercase and has been visited, continue search
            continue;
          }
          nextPaths.push([...currentPath, end]);
        }
      }
    }
    paths = [...nextPaths];
  }
  return pathCount;
}

solve().then(v => console.log(v));
