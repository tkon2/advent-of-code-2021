const readfile = require('../common/readfile');

const solve = async () => {
  const lines = await readfile('./data.txt');

  const map = lines.map(l => l.split(''));


  const getStr = (m) => {
    return m.map(l => l.join('')).join(',');
  }

  const step = (m) => {
    const newM = [...m.map(l => [...l])];
    for (let i = 0; i < m.length; i++) {
      for (let j = 0; j < m[i].length; j++) {
        const r = (j + 1) % m[i].length;
        if (m[i][j] === '>') {
          if (m[i][r] === '.') {
            newM[i][j] = '.';
            newM[i][r] = '>';
          }
        }
      }
    }
    const midMap = [...newM.map(l => [...l])];
    for (let i = 0; i < m.length; i++) {
      for (let j = 0; j < m[i].length; j++) {
        const d = (i + 1) % m.length;
        if (midMap[i][j] === 'v') {
          if (midMap[d][j] === '.') {
            newM[i][j] = '.';
            newM[d][j] = 'v';
          }
        }
      }
    }
    return newM;
  }

  let i = 0;
  let oldStr = '';
  let newStr = getStr(map);
  let currMap = [...map.map(l => [...l])]
  while (oldStr !== newStr) {
    i += 1;
    oldStr = newStr;
    currMap = step(currMap);
    newStr = getStr(currMap);
  }

  return i;

}

solve().then(v => console.log(v));
