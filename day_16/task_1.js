const readfile = require('../common/readfile');

const solve = async () => {
  const lines = await readfile('./data.txt');
  let line = lines[0]; // single line
  let G = '';
  let versionNums = 0;

  for (let i = 0; i < line.length; i += 2) {
    const hex = line.charAt(i) + line.charAt(i + 1);
    G = G + (parseInt(hex, 16).toString(2)).padStart(8, '0');
  }

  const get = (num) => {
    const chars = G.substring(0, num);
    G = G.substring(num);
    return chars === '' ? '0' : chars;
  }

  const getInt = (num) => {
    const n = get(num);
    return parseInt(n === '' ? '0' : n, 2);
  }

  const solveLiteral = () => {
    let bits = 0;
    let num = '';
    do {
      num = get(5);
      bits += 5;
    } while (num[0] === '1');
    return bits;
  }

  const solveOperator = () => {
    const type = get(1);
    let len = 15;
    if (type === '1') len = 11;
    const length = getInt(len);
    let i = 0;
    let bits = 0;
    while (true) {
      if (type === '1' && i >= length) {
        break;
      } else if (type === '0' && bits >= length) {
        break;
      }
      const vNum = getInt(3);
      versionNums += vNum;
      const opType = getInt(3);
      bits += 6;
      if (opType === 4) {
        bits += solveLiteral();
      } else {
        bits += solveOperator();
      }
      i += 1;
    }
    return bits;
  }

  while (G.length >= 3 && G.substring(0, 3) !== '000') {
    const version = getInt(3);
    versionNums += version;
    const type = getInt(3);
    if (type === 4) {
      solveLiteral();
    } else {
      solveOperator();
    }

  }

  return versionNums;

}

solve().then(v => console.log(v));
