// previous guess: 647662851980
const readfile = require('../common/readfile');

const solve = async () => {
  const lines = await readfile('./data.txt');
  let line = lines[0]; // single line
  let G = '';
  let versionNums = 0;

  for (let i = 0; i < line.length; i += 2) {
    const hex = line.charAt(i) + line.charAt(i + 1);
    G = G + (parseInt(hex, 16).toString(2)).padStart(8, '0');
  }

  const get = (num) => {
    const chars = G.substring(0, num);
    G = G.substring(num);
    return chars === '' ? '0' : chars;
  }

  const getInt = (num) => {
    const n = get(num);
    return parseInt(n === '' ? '0' : n, 2);
  }

  const solveLiteral = () => {
    let bits = 0;
    let value = '';
    let num = '';
    do {
      num = get(5);
      value += num.substring(1);
      bits += 5;
    } while (num[0] === '1');
    return { bits, value: parseInt(value, 2) };
  }

  const solveOperator = (operator, depth = 0) => {
    let myBits = 6; // for the header
    const type = get(1);
    myBits += 1;
    let len = 15;
    if (type === '1') len = 11;
    const length = getInt(len);
    myBits += len;
    let values = [];
    let i = 0;
    let bits = 0;
    while (true) {
      if (type === '1' && i >= length) {
        break;
      } else if (type === '0' && bits >= length) {
        break;
      }
      const vNum = getInt(3);
      versionNums += vNum;
      const opType = getInt(3);
      bits += 6;
      let ans = { bits: 0, value: 0 };
      if (opType === 4) {
        ans = solveLiteral();
      } else {
        ans = solveOperator(opType, depth + 1);
      }
      bits += ans.bits;
      values.push(ans.value);
      i += 1;
    }

    let value = 0;
    if (operator === 0) {
      value = values.reduce((acc, curr) => acc + curr, 0);
    } else if (operator === 1) {
      value = values.reduce((acc, curr) => acc * curr, 1);
    } else if (operator === 2) {
      value = values.reduce((acc, curr) => Math.min(acc, curr), Infinity);
    } else if (operator === 3) {
      value = values.reduce((acc, curr) => Math.max(acc, curr), 0);
    } else if (operator === 5) {
      value = values.length >= 2 && values[0] > values[1] ? 1 : 0;
    } else if (operator === 6) {
      value = values.length >= 2 && values[0] < values[1] ? 1 : 0;
    } else if (operator === 7) {
      value = values[0] === values[1] ? 1 : 0;
    }

    return { bits: myBits + bits, value };
  }

  acc = 0;

  while (G.split('').some(g => g === '1')) {
    const version = getInt(3);
    versionNums += version;
    const type = getInt(3);
    let ans = { bits: 0, value: 0 };
    if (type === 4) {
      ans = solveLiteral();
    } else {
      ans = solveOperator(type);
    }

    acc += ans.value;
  }


  return acc;

}

solve().then(v => console.log(v));
