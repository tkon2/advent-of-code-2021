const readfile = require('../common/readfile');

const INSTRUCTION_LOOP_LENGTH = 18; // Is this the same for everyone?
const ADD_A = 5; // Location of add x {ADD_A}
const ADD_B = 15; // Location of add y {ADD_B}

const solve = async () => {
  const lines = await readfile('./data.txt');

  const getRatios = () => {
    let ratios = [];
    let stack = [];
    let a = null;
    let b = null;
    for (let i = 0; i < lines.length; i++) {
      let loop = i % INSTRUCTION_LOOP_LENGTH;
      let loopNum = Math.floor(i / INSTRUCTION_LOOP_LENGTH)
      if (loop == ADD_A) {
        a = lines[i].split(' ')[2];
      }
      if (loop == ADD_B) {
        b = lines[i].split(' ')[2];
      }
      if (loop === INSTRUCTION_LOOP_LENGTH - 1) {
        if (a > 0) {
          stack.push({ num: loopNum, val: b });
        } else {
          const start = stack.pop();
          ratios.push({
            start: start.num,
            end: loopNum,
            ratio: parseInt(start.val, 10) + parseInt(a, 10)
          })
        }
        a = null;
        b = null;
      }
    }
    return ratios;
  }

  const calculate = (n) => {
    let w = 0, x = 0, y = 0, z = 0;
    let currN = 0;
    const getN = () => {
      if (`${n}`.length <= currN) {
        throw new Error("Numbers out of range");
      }
      const newN = parseInt(`${n}`.charAt(currN), 10);
      currN += 1;
      return newN;
    }
    const getV = (v) => {
      if (v === 'w') return w;
      if (v === 'x') return x;
      if (v === 'y') return y;
      if (v === 'z') return z;
      return parseInt(v, 10);
    }
    const setV = (v, val) => {
      if (v === 'w') w = val;
      if (v === 'x') x = val;
      if (v === 'y') y = val;
      if (v === 'z') z = val;
    }
    for (let line of lines) {
      const [operator, a, b] = line.split(' ');
      switch (operator) {
        case 'inp':
          setV(a, getN());
          // console.log(z)
          break;
        case 'add':
          setV(a, getV(a) + getV(b))
          break;
        case 'mul':
          setV(a, getV(a) * getV(b))
          break;
        case 'div':
          setV(a, Math.trunc(getV(a) / getV(b)))
          break;
        case 'mod':
          setV(a, getV(a) % getV(b))
          break;
        case 'eql':
          setV(a, getV(a) === getV(b) ? 1 : 0)
          break;
      }
      // console.log(line.padEnd(10, ' '), `[${w}, ${x}, ${y}, ${z}]`);
    }
    return { w, x, y, z };
  }



  // return calculate(4);

  const valids = [];

  const populateValids = (ratios, number = '11111111111111') => {
    const newRatios = [...ratios];
    if (ratios.length > 0) {
      const ratio = newRatios.pop();
      const r = ratio.ratio;
      let start = 1;
      let end = 9;
      if (r < 0) {
        start = Math.abs(r) + 1;
      } else {
        end = end - r;
      }
      for (let i = start; i <= end; i++) {
        let newNum = number.split('');
        newNum[ratio.start] = i;
        newNum[ratio.end] = i + r;
        newNum = newNum.join('');
        populateValids(newRatios, newNum);
      }
    } else {
      const valid = calculate(parseInt(number)).z === 0;
      if (valid) {
        valids.push(parseInt(number));
      }
    }
  }

  const ratios = getRatios();
  populateValids(ratios);
  return valids[valids.length - 1];
}

solve().then(v => console.log(v));
