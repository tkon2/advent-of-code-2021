const readfile = require('../common/readfile');

const sumOfNum = (n) => {
  return (n * (n+1)) / 2;
}

const solve = async () => {
  const raw = await readfile('./data.txt');
  
  const raw_crabs = raw[0].split(',');

  const crabs = [];
  for (let i = 0; i < raw_crabs.length; i++) {
    const index = parseInt(raw_crabs[i], 10);
    crabs[index] = !crabs[index] ? 1 : crabs[index] + 1;
  }
  
  let minCost = Infinity;
  for (let i = 0; i < crabs.length; i++) {
    let cost = 0;
    for (let c = 0; c < crabs.length; c++) {
      cost += (crabs[c] || 0) * sumOfNum(Math.abs(c - i)); // Number of crabs at position times distance
    }
    minCost = Math.min(minCost, cost);
  }
  return minCost;

}

solve().then(v => console.log(v));
