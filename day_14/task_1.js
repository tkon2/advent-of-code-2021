const readfile = require('../common/readfile');
const removeDupes = require('../common/duplicates');

const solve = async () => {
  const lines = await readfile('./data.txt');

  let D = lines[0].split('');
  let C = lines.splice(2).map(l => l.split(' -> '));

  for (let r = 0; r < 10; r++) {
    DT = [];
    for (let d = 1; d < D.length; d++) {
      const a = D[d-1];
      const b = D[d];
      if (d === 1) {
        DT.push(a);
      }
      const c = C.find(y => y[0] === a + b)[1];
      if (c) {
        DT.push(c);
      }
      DT.push(b);
    }
    D = [...DT];
  }

  const F = {};
  for (let d = 0; d < D.length; d++) {
    const Dv = D[d];
    if (F[Dv]) {
      F[Dv] += 1;
    } else {
      F[Dv] = 1;
    }
  }

  const P = Object.entries(F).sort((a,b) => a[1] - b[1])
  return P[P.length-1][1] - P[0][1];

  
}

solve().then(v => console.log(v));
