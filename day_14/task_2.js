
const readfile = require('../common/readfile');
const removeDupes = require('../common/duplicates');

const solve = async () => {
  const lines = await readfile('./data.txt');

  let D = lines[0].split('');
  let C = lines.splice(2).map(l => l.split(' -> '));
  let P = {}; // contains pairs
  for (let d = 1; d < D.length; d++) {
    const a = D[d-1];
    const b = D[d];
    const ab = `${a}${b}`;
    if (P[ab]) {
      P[ab] += 1;
    } else {
      P[ab] = 1;
    }
  }


  for (let r = 0; r < 40; r++) {
    Rn = {};
    const pairs = Object.entries(P);
    for (let p = 0; p < pairs.length; p++) {
      const pair = pairs[p];
      const rule = C.find(y => y[0] === pair[0])[1];
      const pairA = pair[0].charAt(0) + rule;
      const pairB = rule + pair[0].charAt(1);
      if (Rn[pairA]) {
        Rn[pairA] += pair[1];
      } else {
        Rn[pairA] = pair[1];
      }
      if (Rn[pairB]) {
        Rn[pairB] += pair[1];
      } else {
        Rn[pairB] = pair[1];
      }
    }
    P = Rn;
  }

  const S = {[D[0]]: 1}; // solved - Add the first char
  const pairs = Object.entries(P);
  for (let p = 0; p < pairs.length; p++) {
    const pair = pairs[p];
    const Dv = pair[0].charAt(1); // Add the second char
    if (S[Dv]) {
      S[Dv] += pair[1];
    } else {
      S[Dv] = pair[1];
    }
  }

  const So = Object.entries(S).sort((a,b) => a[1] - b[1])
  return So[So.length-1][1] - So[0][1];

  
}

solve().then(v => console.log(v));
