
const readfile = require('../common/readfile');

const solve = async () => {
  const lines = await readfile('./data.txt');
  const d = lines.map(l => l.split(''));

  const getNeighbours = (x,y) => {
    const pos = [];
    for (let i = -1; i < 2; i++) {
      for (let j = -1; j < 2; j++) {
        if (x+i >= 0 && x+i < d.length) {
          if (y+j >= 0 && y+j < d[x+i].length) {
            pos.push([x+i,y+j]);
          }
        }
      }
    }
    return pos;
  }

  const getVal = (x,y) => {
    const val = d[x][y];
    if (val !== 'h' && val !== 'w') {
      return parseInt(val, 10);
    }
    return val;
  }

  let iter = 0;
  while (d.some(i => i.some(j => `${j}` !== '0'))) {
    iter += 1;

    // Find explodees step
    for (let i = 0; i < d.length; i++) {
      for (let j = 0; j < d[i].length; j++) {
        const e = getVal(i,j);
        if (e === 'w') {
          ;
        } else if (e === 9) {
          d[i][j] = 'w'; // [w]ill explode
        } else {
          d[i][j] =  `${e + 1}`;
        }
      }
    }

    // Chain reaction step
    while(d.some(i => i.some(j => j === 'w'))) {
      for (let i = 0; i < d.length; i++) {
        for (let j = 0; j < d[i].length; j++) {
          if (d[i][j] === 'w') {
            d[i][j] = 'h'; // [h]as exploded
            const N = getNeighbours(i,j);
            for (let n = 0; n < N.length; n++) {
              const x = N[n][0];
              const y = N[n][1];
              const ne = getVal(x,y);
              if (ne !== 'h' && ne !== 'w') {
                if (ne === 9) {
                  d[x][y] = 'w';
                } else {
                  d[x][y] = `${ne + 1}`;
                }
              }
            }
          }
        }
      }
    }
    
    // Reset all exploded squid
    for (let i = 0; i < d.length; i++) {
      for (let j = 0; j < d[i].length; j++) {
        if (d[i][j] === 'h') {
          d[i][j] = '0';
        }
      }
    }
  }
  return iter;
}

solve().then(v => console.log(v));


